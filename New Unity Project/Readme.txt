Um Game de teste para uma vaga de programador junior na empresa hermit crab studio, o prototipo consiste em um game de embaixadas,
onde o jogador n�o deve deixar a bola tocar no ch�o, para isso ele deve clicar no circulo que aparece ao lado no personagem
o circulo tem cores indicando qu�o bom � a posi��o da bola para toca-la, a cor vermelha representa que esta muita alta, a amarela que est� intermediaria
e a verde que est� em uma altura boa. Cada altura representa um comportamento diferente na bola, o ideal � sempre clicar quando a cor estiver verde,
assim a bola n�o foge para longe do personagem.
Cada vez que a bola ficar mais longe do personagem o circulo ficar� menor, e se a bola ficar muito longe o circulo n�o aparece mais.

Para compilar escolha a op��o para PC e compile, pode ser jogado em modo janela ou fullscreen. Vers�o Unity 5.6.2f1

O jogador para vencer deve fazer mais que 40 toques na bola, e a cada n�mero especifico de toques a bola vai ficando um pouco mais pesada para dificultar.
O circulo onde o jogador precisa clicar para levantar a bola surgiu como inspira��o do video que foi colocado como Exemplo da Ideia, pois reflete a
venda nos olhos, assim o jogador precisa ficar mais atento onde o circulo ir� aparecer, ou seja, o tamanho e o lado, al�m das cores.

Prot�tipo desenvolvido por Frederico Andros da Silva
