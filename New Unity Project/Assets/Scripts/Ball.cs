﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    //Verifica se existe colisão com o chão
    private void OnCollisionEnter(Collision collision)
    {
        GameObject gamePlayAux = GameObject.Find("GamePlay");
        if (collision.gameObject.name == "Ground(Clone)" && gamePlayAux.GetComponent<Gameplay>().getIsPlaing() == true)
        {      
            //Caso colia o jogador perde e chama a função para criar os botões de reiniciar e voltar
            gamePlayAux.GetComponent<Gameplay>().Loser(true);
        }
    }

}
