﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
public class MainMenu : MonoBehaviour {

    private bool quit;
    private void Start()
    {
        quit = false;
    }
    private void Update()
    {
        Debug.Log(quit);
        if(quit == true)
        {
            Application.Quit();
        }
    }

    public void changeSceneTo(string nextScene)
    {
        GameObject managerAux = GameObject.Find("Manager");
        if (nextScene == "exit")
        {
            Debug.Log("saindo");
            quit = true;
        }
        else
        {
            SceneManager.LoadScene(nextScene);
            Debug.Log(nextScene);
        }
    }
}
