﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Gameplay : MonoBehaviour {

    /// <summary>
    /// Um Game de teste para uma vaga de programador junior na empresa hermit crab studio, o prototipo consiste em um game de embaixadas, onde o jogador não deve deixar a bola tocar no chão, para isso
    /// ele deve clicar no circulo que aparece ao lado no personagem, o circulo tem cores indicando quão bom é a posição da bola para toca-la, a cor vermelha representa que esta muita alta, a amarela que está intermediaria
    /// e a verde que está em uma altura boa. Cada altura representa um comportamento diferente na bola, o ideal é sempre clicar quando a cor estiver verde, assim a bola não foge para longe do personagem.
    /// Cada vez que a bola ficar mais longe do personagem o circulo ficará menor, e se a bola ficar muito longe o circulo não aparece mais.
    /// </summary>
    /// 

    // Declarando ps prefabs
    public GameObject groundPrefab;
    public GameObject playerPrefab;
    public GameObject ballPrefab;

    // Declarando os objetos na cena
    public GameObject targetPrefab;
    public GameObject buttonRestartPrefab;
    public GameObject buttonBackPrefab;

    private GameObject ground;
    private GameObject player;
    private GameObject ball;

    private GameObject target;
    private GameObject buttonRestart;
    private GameObject buttonBack;

    private GameObject canvas;
    private GameObject eventSystem;
    
    private GameObject textStart;
    private GameObject textRestart;
    private GameObject textScore;

    // Declarando as variaveis usadas como contadores
    private int cont;
    private int contStart;
    private int heightBallColor;
    private int score;
    private int massBall;

    // Declarando as variaveis usadas para verificação
    private bool targetOk;
    private bool targetOn;
    private bool isPlaing;
    private bool isDown;
    private bool isRight;

    // Declarando as variaveis de posição
    private float posX;
    private float posY;
    private float ballPosYPre;
    private float scale;

    // Use this for initialization
    void Start () {

        //Instanciando os objetos
        ground = Instantiate(groundPrefab); // chão que ficará invisivel, somente para detectar colisão

        player = Instantiate(playerPrefab); // personagem

        ball = Instantiate(ballPrefab); // bola
        massBall = 1;
        ball.GetComponent<Rigidbody>().mass = massBall; // setando a massa da bola, ela mudará ao longo do jogo para dificultar um pouco o jogador

        //Criando o canvas que receberá os botões e textos em jogo
        canvas = new GameObject("Canvas");
        canvas.AddComponent<Canvas>();
        canvas.AddComponent<CanvasRenderer>();
        canvas.AddComponent<GraphicRaycaster>();
        canvas.AddComponent<CanvasScaler>();
        canvas.GetComponent<Transform>().position = new Vector3(0.0f, 0.0f, 50.0f);
        canvas.GetComponent<RectTransform>().anchorMin = new Vector2(0.0f, 0.0f);
        canvas.GetComponent<RectTransform>().anchorMax = new Vector2(1.0f, 1.0f);
        canvas.GetComponent<RectTransform>().offsetMin = new Vector2(10.0f, 10.0f);
        canvas.GetComponent<RectTransform>().offsetMax = new Vector2(-10.0f, -10.0f);

        eventSystem = new GameObject("EventSystem");
        eventSystem.AddComponent<EventSystem>();
        eventSystem.AddComponent<StandaloneInputModule>();

        //Instanciando o circulo onde o jogador deverá clicar para erguer a bola
        target = Instantiate(targetPrefab);
        target.transform.SetParent(canvas.transform);
        target.SetActive(false); // Setando ele como inativo na cena

        // Inicializando as variaveis de contagem
        cont = 0;
        contStart = 0;
        ballPosYPre = 0.0f;
        scale = 0.0f;
        heightBallColor = 0;
        score = 0;

        // inicializando as variaveis de verivicação
        targetOk = false;
        targetOn = false;
        isPlaing = false;
        isDown = false;
        isRight = false;

        //Criando e inicializando o texto do score onde marcará os toques na bola
        textScore = new GameObject("TextScore");
        textScore.transform.SetParent(canvas.transform);
        textScore.AddComponent<Text>();
        textScore.GetComponent<RectTransform>().localPosition = new Vector3(40.0f, 20.0f, 50.0f);
        textScore.GetComponent<Text>().font = Resources.Load<Font>("fontStartGame");
        textScore.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
        textScore.GetComponent<Text>().text = "Score: " + score;
        textScore.GetComponent<Text>().fontSize = 14;
        textScore.GetComponent<Text>().color = new Color(0, 0, 255);

    }

    // Update is called once per frame
    void Update() {
        //Fazendo a contagem para iniciar o jogo, a cada tempo aparece um número da contagem regressiva
        countdown();

        //Verifica se esta em jogo
        if (isPlaing == true)
        {
            textScore.GetComponent<Text>().text = "Score: " + score;// Atualiza o score
            plaing();
           
        }
     
    }

    public void countdown()
    {
        if (contStart < 420)
        {
            contStart++;
        }
        if (contStart == 15)
        {
            textStart = new GameObject("TextStart");
            textStart.transform.SetParent(canvas.transform);
            textStart.AddComponent<Text>();
            textStart.GetComponent<RectTransform>().localPosition = new Vector3(0.0f, 0.0f, 0.0f);
            textStart.GetComponent<Text>().font = Resources.Load<Font>("fontStartGame");
            textStart.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
            textStart.GetComponent<Text>().text = "1";
            textStart.GetComponent<Text>().fontSize = 20;
            textStart.GetComponent<Text>().color = new Color(255, 0, 0);
        }
        if (contStart == 115)
        {
            textStart.GetComponent<Text>().text = "2";
        }
        if (contStart == 215)
        {
            textStart.GetComponent<Text>().text = "3";
        }
        if (contStart == 315)
        {
            textStart.GetComponent<Text>().text = "GO";
        }
        if (contStart == 415)
        {
            Destroy(textStart);// Destroi da cena o texto de contagem regressiva
            ball.GetComponent<Rigidbody>().AddForce(new Vector3(0.0f, 20.0f, 0.0f), ForceMode.Impulse);// Impulsiona a bola para cima
            this.isPlaing = true; // Seta como verdade que o jogo iniciou
        }
    }

    //Função de Jogo
    public void plaing()
    {

        //Verifica se a bola esta subindo ou descendo
        if (isDown == false)
        {
            //Compara a posição da bola atual com a anterior, se o valor atual for menor que a anterior seta a variavel isDown como true, a bola está descendo
            if (ball.GetComponent<Transform>().position.y > ballPosYPre)
            {
                ballPosYPre = ball.GetComponent<Transform>().position.y;
                isDown = false;
            }
            else
            {
                isDown = true;
            }
        }
        //Verifica se a bola esta descendo para que possa ativar o circulo do target
        if (isDown == true)
        {
            //Verifica se a bola esta dentro do alcance do personagem no eixo X
            if (ball.GetComponent<Transform>().position.x > -1.5f && ball.GetComponent<Transform>().position.x < 1.5f)
            {
                //Verifica se a bola esta proxima do personagem no eixo Z
                if (ball.GetComponent<Transform>().position.z > 25.0f && ball.GetComponent<Transform>().position.z < 27.0f)
                {
                    //Verifica em que altura a bola se encontra, cada um dos IF representa um Range de altura, para modificar a cor do circulo e atribuir a ação na bola
                    if (ball.GetComponent<Transform>().position.y > 0.50f && ball.GetComponent<Transform>().position.y < 2.50f && targetOn == false)
                    {
                        target.SetActive(true); // Ativa o circulo Target
                                                // Verifica em qual dos lados do personagem a bola está para posicionar o circulo Target
                        if (ball.GetComponent<Transform>().position.x > -1.5f && ball.GetComponent<Transform>().position.x <= 0.0f)
                        {
                            posX = -5.0f;
                            isRight = false;
                        }
                        if (ball.GetComponent<Transform>().position.x > 0.0f && ball.GetComponent<Transform>().position.x <= 1.5f)
                        {
                            posX = 5.0f;
                            isRight = true;
                        }
                        //Posiciona o Target
                        posY = -10.0f;
                        target.GetComponent<RectTransform>().localPosition = new Vector3(posX, posY, 0.0f);
                        targetOn = true;
                    }
                    //Verifica o quanto a bola está longe do personagem, quanto mais longe menor o circulo ficará
                    if (ball.GetComponent<Transform>().position.x > -1.10f && ball.GetComponent<Transform>().position.x <= -0.7f || ball.GetComponent<Transform>().position.x > 0.7f && ball.GetComponent<Transform>().position.x <= 1.1f)
                    {
                        scale = 0.85f;
                        target.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);
                    }
                    if (ball.GetComponent<Transform>().position.x > -0.7f && ball.GetComponent<Transform>().position.x <= -0.3f || ball.GetComponent<Transform>().position.x > 0.3f && ball.GetComponent<Transform>().position.x <= 0.7f)
                    {
                        scale = 1.5f;
                        target.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);
                    }
                    if (ball.GetComponent<Transform>().position.x > -0.3f && ball.GetComponent<Transform>().position.x <= 0.0f || ball.GetComponent<Transform>().position.x > 0.0f && ball.GetComponent<Transform>().position.x <= 0.3f)
                    {
                        scale = 2.5f;
                        target.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);
                    }
                    //Verifica a altura da bola para determinar a cor do circulo, neste caso a cor verde
                    if (ball.GetComponent<Transform>().position.y > 0.50f && ball.GetComponent<Transform>().position.y < 1.50f)
                    {
                        ColorBlock colors = target.GetComponent<Button>().colors;
                        colors.normalColor = new Color(0.0f, 255.0f, 0.0f);
                        colors.highlightedColor = new Color(0.0f, 255.0f, 0.0f);
                        target.GetComponent<Button>().colors = colors;
                        heightBallColor = 1;

                    }
                    //Verifica a altura da bola para determinar a cor do circulo, neste caso a cor amarela
                    if (ball.GetComponent<Transform>().position.y >= 1.50f && ball.GetComponent<Transform>().position.y < 2.00f)
                    {
                        ColorBlock colors = target.GetComponent<Button>().colors;
                        colors.normalColor = new Color(155.0f, 155.0f, 0.0f);
                        colors.highlightedColor = new Color(155.0f, 155.0f, 0.0f);
                        target.GetComponent<Button>().colors = colors;
                        heightBallColor = 2;
                    }
                    //Verifica a altura da bola para determinar a cor do circulo, neste caso a cor vermelha
                    if (ball.GetComponent<Transform>().position.y >= 2.00f && ball.GetComponent<Transform>().position.y < 2.50f)
                    {
                        ColorBlock colors = target.GetComponent<Button>().colors;
                        colors.normalColor = new Color(255.0f, 0.0f, 0.0f);
                        colors.highlightedColor = new Color(255.0f, 0.0f, 0.0f);
                        target.GetComponent<Button>().colors = colors;
                        heightBallColor = 3;

                    }
                    //Verifica se a bola está fora do alcance do personagem em relação a altura, caso esteja o circulo é desativado
                    if (ball.GetComponent<Transform>().position.y <= 0.50f || ball.GetComponent<Transform>().position.y >= 2.50f)
                    {
                        target.SetActive(false);
                        heightBallColor = 0;
                        isRight = false;
                        targetOn = false;
                    }
                }

            }

        }
    }
    //Função que verifica se o Jogador perdeu ou ganhou
    public void Loser(bool isLoser)
    {
        isPlaing = false;
        //Perdedor
        if (isLoser == true)
        {            
            textRestart = new GameObject("TextRestart");
            textRestart.transform.SetParent(canvas.transform);
            textRestart.AddComponent<Text>();
            textRestart.GetComponent<Text>().font = Resources.Load<Font>("fontStartGame");
            textRestart.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
            textRestart.GetComponent<Text>().text = "Lost";
            textRestart.GetComponent<Text>().fontSize = 15;
            textRestart.GetComponent<Text>().color = new Color(255, 0, 0);
            textRestart.GetComponent<RectTransform>().localPosition = new Vector3(0.0f, 7.0f, 0.0f);
        }
        //Ganhador
        if (isLoser == false)
        {
            textRestart = new GameObject("TextRestart");
            textRestart.transform.SetParent(canvas.transform);
            textRestart.AddComponent<Text>();
            textRestart.GetComponent<Text>().font = Resources.Load<Font>("fontStartGame");
            textRestart.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
            textRestart.GetComponent<Text>().text = "WIN";
            textRestart.GetComponent<Text>().fontSize = 15;
            textRestart.GetComponent<Text>().color = new Color(255, 0, 0);
            textRestart.GetComponent<RectTransform>().localPosition = new Vector3(0.0f, 7.0f, 0.0f);
        }
        //Instancia o texto na tela e os botões de reiniciar ou voltar para o menu
        this.buttonRestart = Instantiate(buttonRestartPrefab);
        this.buttonRestart.transform.SetParent(canvas.transform);
        this.buttonRestart.GetComponent<RectTransform>().localPosition = new Vector3(30.0f, -20.0f, 50.0f);
        this.buttonRestart.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);

        this.buttonBack = Instantiate(buttonBackPrefab);
        this.buttonBack.transform.SetParent(canvas.transform);
        this.buttonBack.GetComponent<RectTransform>().localPosition = new Vector3(30.0f, -40.0f, 50.0f);
        this.buttonBack.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }
    //Função que retorna se o jogo está ou não verdadeiro   
    public bool getIsPlaing()
    {
        return this.isPlaing;
    }
    // Função usada nos botões de reiniciar partida e voltar para o menu
    public void changeSceneTo(string scene)
    {
        this.gameObject.GetComponent<MainMenu>().changeSceneTo(scene);
    }

    //Função de toque na bola
    public void shoot(bool shooting)
    {
        GameObject gamePlayAux = GameObject.Find("GamePlay");// Pega o GamePlay da cena
        //Como é uma função atribuida a um botão pelo prefab, o codigo não reconhecia o GamePlay deste script, por isso foi necessario pegar o da cena com o metodo acima
        gamePlayAux.GetComponent<Gameplay>().target.SetActive(false);// Desativa o Target
        gamePlayAux.GetComponent<Gameplay>().targetOn = false;// Certifica que o Target não aparece na cena
        gamePlayAux.GetComponent<Gameplay>().isDown = false;//Define que a bola não está mais descendo
        gamePlayAux.GetComponent<Gameplay>().ballPosYPre = 0.0f;//Zera a posição de parametro da altura da bola
        gamePlayAux.GetComponent<Gameplay>().score++;// incrementa no score uma vez

        //Criando e iniciando as variaveis de dentro da função 
        float axisX = 0.0f;
        float axisY = 0.0f;
        float axisZ = 0.0f;

        //Este conjunto de IFs serve para incrementar a massa da bola para dificultar o jogador, a cada score alcançado a massa altera para cima
        if (gamePlayAux.GetComponent<Gameplay>().score > 1 && gamePlayAux.GetComponent<Gameplay>().score <= 10)
        {
            gamePlayAux.GetComponent<Gameplay>().ball.GetComponent<Rigidbody>().mass = 2;
        }
        if (gamePlayAux.GetComponent<Gameplay>().score > 10 && gamePlayAux.GetComponent<Gameplay>().score <= 20)
        {
            gamePlayAux.GetComponent<Gameplay>().ball.GetComponent<Rigidbody>().mass = 3;
        }
        if (gamePlayAux.GetComponent<Gameplay>().score > 20 && gamePlayAux.GetComponent<Gameplay>().score <= 30)
        {
            gamePlayAux.GetComponent<Gameplay>().ball.GetComponent<Rigidbody>().mass = 4;
        }
        if (gamePlayAux.GetComponent<Gameplay>().score > 30 && gamePlayAux.GetComponent<Gameplay>().score <= 40)
        {
            gamePlayAux.GetComponent<Gameplay>().ball.GetComponent<Rigidbody>().mass = 5;
        }
        //Caso o jogador atinja 40 toque ele vence
        if (gamePlayAux.GetComponent<Gameplay>().score > 40)
        {
            gamePlayAux.GetComponent<Gameplay>().Loser(false);
        }

        //As proximas condições servem para determinar o comportamento da bola
        //Este a bola está na cor verde, por isso a tendencia é que ela não vá muito alto e nem muito longe do personagem
        if (gamePlayAux.GetComponent<Gameplay>().ball.GetComponent<Transform>().position.y > 0.50f && gamePlayAux.GetComponent<Gameplay>().ball.GetComponent<Transform>().position.y <= 1.50f)
        {
            axisY = Random.Range(25.0f, 35.0f);
            axisZ = Random.Range(0.0f, 3.5f);
            if (gamePlayAux.GetComponent<Gameplay>().isRight == false)
            {                
                axisX = Random.Range(0.0f, 3.5f);                
            }
            if (gamePlayAux.GetComponent<Gameplay>().isRight == true)
            {
                axisX = Random.Range(-3.5f, 0.0f);
            }
        }
        //Esta proxima condição já defini um comportamento mais inconstante para a bola
        if (gamePlayAux.GetComponent<Gameplay>().ball.GetComponent<Transform>().position.y > 1.50f && gamePlayAux.GetComponent<Gameplay>().ball.GetComponent<Transform>().position.y <= 2.00f)
        {
            axisY = Random.Range(35.0f, 45.0f);
            axisZ = Random.Range(3.5f, 13.5f);
            if (gamePlayAux.GetComponent<Gameplay>().isRight == false)
            {
                axisX = Random.Range(3.5f, 13.5f);
            }
            if (gamePlayAux.GetComponent<Gameplay>().isRight == true)
            {
                axisX = Random.Range(-13.5f, -3.5f);
            }
        }
        //E por fim na cor vermelha a bola se comporta bem mais inesperada
        if (gamePlayAux.GetComponent<Gameplay>().ball.GetComponent<Transform>().position.y > 2.0f && gamePlayAux.GetComponent<Gameplay>().ball.GetComponent<Transform>().position.y <= 2.50f)
        {
            axisY = Random.Range(45.0f, 55.0f);
            axisZ = Random.Range(13.5f, 35.5f);
            if (gamePlayAux.GetComponent<Gameplay>().isRight == false)
            {
                axisX = Random.Range(13.5f, 35.5f);
            }
            if (gamePlayAux.GetComponent<Gameplay>().isRight == true)
            {
                axisX = Random.Range(-35.5f, -13.5f);
            }
        }
        //Aplica uma força de impulso na bola
        gamePlayAux.GetComponent<Gameplay>().ball.GetComponent<Rigidbody>().AddForce(new Vector3(axisX, axisY, axisZ), ForceMode.Impulse);

    }

}
